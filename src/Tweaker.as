const string title = "\\$d36" + Icons::Wrench + "\\$z Tweaker";

Mania@ game;
Window@ window;
UI::Texture@ advert;

string advertPath = "src/Resources/Advert.png";

void RenderMenu() {
    if (!Setting_EmbeddedInterface && UI::MenuItem(title, "", window.isOpened)) {
        window.isOpened = !window.isOpened;
    }
}

void RenderInterface()
{
    if (window.isOpened) {
		window.Render();
		game.ApplySettings();
	}
}

void Render()
{
	game.Render();
	game.Routine();
	if (game.initialised) {
		if (game.app.GameScene is null) {
			game.RemoveNods();
		}
	} else {
		game.AddNods();
	}
}

void Update(float dt)
{
	game.Update(dt);
}

UI::InputBlocking OnKeyPress(bool down, VirtualKey key)
{
	return game.OnKeyPress(down, key);
}

void Main()
{
	@advert = UI::LoadTexture(advertPath);
	@game = Mania();
	@window = Window();
}
