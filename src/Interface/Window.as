class Window
{
    bool isOpened = false;

    array<Tab@> tabs;
    Tab@ activeTab;
    Tab@ lastActiveTab;

    // Main Tabs
    Window()
    {
        AddTab(GraphicsTab());
#if !TMNEXT
        AddTab(EnvironmentTab());
#endif
        AddTab(InterfaceTab());
#if TMNEXT
#if SIG_REGULAR
        AddTab(SystemTab());
#endif
#endif
    }

    void AddTab(Tab@ tab, bool select = false){
        tabs.InsertLast(tab);
        if (select) {
            @activeTab = tab;
        }
    }

    void Render()
    {
        UI::SetNextWindowSize(800, 480, UI::Cond::FirstUseEver);
        if (UI::Begin(title, isOpened)){
            RenderContents();
        }
        UI::End();
    }

    void RenderContents()
    {
        // Push the last active tab style so that the separator line is colored (this is drawn in BeginTabBar)
        auto previousActiveTab = lastActiveTab;
        if (previousActiveTab !is null) {
            previousActiveTab.PushTabStyle();
        }
        UI::BeginTabBar("Tabs");
        RenderTabContents();
        UI::EndTabBar();

        // Pop the tab style (for the separator line) only after EndTabBar, to satisfy the stack unroller
        if (previousActiveTab !is null) {
            previousActiveTab.PopTabStyle();
        }
    }

    void RenderTabContents(bool noTabs = false)
    {
        for(uint i = 0; i < tabs.Length; i++) {
            auto tab = tabs[i];

            UI::PushID(tab);

            if (noTabs) {
                if (UI::CollapsingHeader(tab.GetLabel())) {
                    tab.Render();
                }
            } else {
                if (!tab.IsVisible()) continue;

                int flags = 0;

                if (tab is activeTab) {
                    flags |= UI::TabItemFlags::SetSelected;
                    if (!tab.GetLabel().Contains("Loading")) @activeTab = null;
                }

                tab.PushTabStyle();

                if (tab.CanClose()){
                    bool open = true;
                    if(UI::BeginTabItem(tab.GetLabel(), open, flags)){
                        @lastActiveTab = tab;

                        UI::BeginChild("Tab");
                        tab.Render();
                        UI::EndChild();

                        UI::EndTabItem();
                    }
                    if (!open){
                        tabs.RemoveAt(i--);
                    }
                } else {
                    if(UI::BeginTabItem(tab.GetLabel(), flags)){
                        @lastActiveTab = tab;

                        UI::BeginChild("Tab");
                        tab.Render();
                        UI::EndChild();

                        UI::EndTabItem();
                    }
                }
                tab.PopTabStyle();
            }
            UI::PopID();
        }
    }
}
