namespace System
{
    const float GeomLodScaleZ_Min = 0;
    const float GeomLodScaleZ_Max = 100;
}

[Setting hidden]
bool Setting_LegacyDefaults = true;

#if SIG_REGULAR
[Setting hidden]
#endif
bool Setting_System_GeomLodScaleZ_Enabled = false;

#if SIG_REGULAR
[Setting hidden]
#endif
float Setting_System_GeomLodScaleZ = 1.000f;
