# Tweaker <sup><sub>Trackmania ManiaPlanet Turbo</sub></sup>
Plugin for Openplanet that helps modify in-game hidden settings and slightly boost the performance.

## Preview Version
You can download upcoming unsigned `0.3.0` version [here](https://openplanet.dev/plugin/tweakerpreview). Keep in mind it **will** conflict with your current version, therefore you must uninstall the old version first. Use at your own risk!

## Features
* Graphics
    * Draw Distance
* Environment
    * Full SkyDome <sup>MP Turbo</sup>
* Interface
    * Overlay Scaling <sup>TM</sup>
    * FPS Counter
## Download
* [OpenPlanet](https://openplanet.nl/files/126)
* [Releases](https://gitlab.com/fentrasLABS/openplanet/tweaker/-/releases)

## Screenshots
![](_git/1.png)
![](_git/2.png)
![](_git/3.png)

## Credits
- *UI tabs* by [Miss](https://github.com/openplanet-nl/plugin-manager/tree/master/src/Interface)
- *Thumbnail background* by [blackpulse](https://trackmania.exchange/maps/54009)
- *Project icon* by [Fork Awesome](https://forkaweso.me/)